<?php
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_BAD_IBLOCK_ID"] = "�������� ������������� ���������";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_EMPTY_ELEMENTS"] = "�� ����� ������ ��������� ��� ���������";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_EMPTY_FIELDS"] = "����������� ��������� ������ ��� ����������";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_BAD_FIELDS"] = "������������ ����� ���������� ������ ��� ����������";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_BAD_CATALOG"] = "�������� �� �������� �������� ���������";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_SECTION_PRODUCTS_UPDATE"] = "��� ���������� ������� ������� [#ID#] #NAME# ��������� ������. ��������� � ������ � ��������� ��������.";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_SECTION_PRODUCTS_CONVERT"] = "��� ����������� ������� ������� [#ID#] #NAME# ��������� ������. ��������� � ������ � ��������� ��������.";
$MESS["BX_CATALOG_PRODUCT_ACTION_ERR_SECTION_SERVICES_CONVERT"] = "��� ����������� ����� ������� [#ID#] #NAME# ��������� ������. ��������� � ������ � ��������� ��������.";
$MESS['BX_CATALOG_PRODUCT_ACTION_ERR_SELECTED_NOT_SIMPLE_PRODUCT'] = "������ #NAMES# �� �������� ��������, ������� �� ����� ���� �������������� � ������.";
$MESS['BX_CATALOG_PRODUCT_ACTION_ERR_SELECTED_NOT_SERVICE'] = "������� #NAMES# ��� �������� ��������. ��� ��� ����������� �� ����� �����������.";
$MESS['BX_CATALOG_PRODUCT_ACTION_ERR_SELECTED_INVENTORY_PRODUCTS'] = "������ #NAMES# ���������� �������������� � ������, ��� ��� ��� ������������ � ���������� ��������� ����������.";
$MESS['BX_CATALOG_PRODUCT_ACTION_ERR_CANNOT_SET_FIELD_BY_TYPE'] = "������ ������������� ��������� ��������� ������� #NAMES#";
