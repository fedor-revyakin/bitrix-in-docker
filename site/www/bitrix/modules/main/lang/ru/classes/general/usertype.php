<?
$MESS["USER_TYPE_ENTITY_ID_MISSING"] = "�� ������ ������.";
$MESS["USER_TYPE_ENTITY_ID_TOO_LONG1"] = "������������� ������� ������� ������� (������ 50-�� ��������).";
$MESS["USER_TYPE_ENTITY_ID_INVALID"] = "������������� ������� �������� ������������ �������. ����������� ��������: A-Z, 0-9 � _.";
$MESS["USER_TYPE_FIELD_NAME_MISSING"] = "�� ������� ��� ����.";
$MESS["USER_TYPE_FIELD_NAME_NOT_UF"] = "��� ���� ���������� �� � UF_";
$MESS["USER_TYPE_FIELD_NAME_TOO_SHORT"] = "��� ���� ������� �������� (������ 4-� ��������).";
$MESS["USER_TYPE_FIELD_NAME_TOO_LONG1"] = "��� ���� ������� ������� (������ 50-�� ��������).";
$MESS["USER_TYPE_FIELD_NAME_INVALID"] = "��� ���� �������� ������������ �������. ����������� ��������: A-Z, 0-9 � _.";
$MESS["USER_TYPE_USER_TYPE_ID_MISSING"] = "�� ������ ���������������� ���.";
$MESS["USER_TYPE_USER_TYPE_ID_INVALID"] = "������ �������� ���������������� ���.";
$MESS["USER_TYPE_ADD_ALREADY_ERROR"] = "���� #FIELD_NAME# ��� ������� #ENTITY_ID# ��� ����������.";
$MESS["USER_TYPE_TABLE_CREATION_ERROR"] = "������ �������� ������� ��� �������� �������� ������� ������� #ENTITY_ID#.";
$MESS["USER_TYPE_ADD_ERROR"] = "������ ���������� #FIELD_NAME# ��� ������� #ENTITY_ID#.";
$MESS["USER_TYPE_UPDATE_ERROR"] = "������ �������������� #FIELD_NAME# ��� ������� #ENTITY_ID#.";
$MESS["USER_TYPE_DELETE_ERROR"] = "������ �������� #FIELD_NAME# ��� ������� #ENTITY_ID#.";
?>