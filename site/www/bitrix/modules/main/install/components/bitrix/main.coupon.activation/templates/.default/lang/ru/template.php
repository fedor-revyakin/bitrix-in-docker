<?php
$MESS['MAIN_COUPON_ACTIVATION_LICENSE_OVER_TITLE'] = '���� �������� ����� �������� ����������';
$MESS['MAIN_COUPON_ACTIVATION_LICENSE_OVER_DESCRIPTION'] = '�������� ��������, ����� ���������� ������. <br>
�������� ��������� ����� �������������� ��� ����� ������ ��������.';
$MESS['MAIN_COUPON_ACTIVATION_BUTTON_RENEW'] = '�������� ��������';
$MESS['MAIN_COUPON_ACTIVATION_TITLE_ACTIVATE_KEY'] = '�������� ��������<br> ��� ������� ��� ��������� �������';
$MESS['MAIN_COUPON_ACTIVATION_LICENSE_KEY_FIELD'] = '������� ��� ���������';
$MESS['MAIN_COUPON_ACTIVATION_BUTTON_NEED_HELP'] = '����� ������?';
$MESS['MAIN_COUPON_ACTIVATION_PLEASE_WAIT'] = '��������� ������� ������';
$MESS['MAIN_COUPON_ACTIVATION_BUTTON_PARTNER'] = '���������� � ��������';
$MESS['MAIN_COUPON_ACTIVATION_BUTTON_REFRESH_PAGE'] = '�������� ��������';
$MESS['MAIN_COUPON_ACTIVATION_NAME_FIELD'] = '���';
$MESS['MAIN_COUPON_ACTIVATION_PHONE_FIELD'] = '�������';
$MESS['MAIN_COUPON_ACTIVATION_EMAIL_FIELD'] = '�����';
$MESS['MAIN_COUPON_ACTIVATION_BUTTON_SEND'] = '���������';
$MESS['MAIN_COUPON_ACTIVATION_TITLE_PARTNER'] = '��������� ������ ������ ��������';
$MESS['MAIN_COUPON_ACTIVATION_SUBTITLE_PARTNER'] = '������� �������� � ���� ����� ��������� ������ ';
$MESS['MAIN_COUPON_ACTIVATION_SUBTITLE_ACTIVATE_KEY'] = '�� ����������� �������� �� �� ����� �������� ������ ����� ������, ���������� �������� ��������. ���� ������ �����������, � �� �������� ���������, <a href="#SUPPORT_LINK#" target="_blank">���������� � ���������</a> �� ����� ��������� � ������� ��� 
� ���� ����.';
$MESS['MAIN_COUPON_ACTIVATION_TITLE_PARTNER_SUCCESS'] = '������ ����������';
$MESS['MAIN_COUPON_ACTIVATION_TITLE_ERROR'] = '��������� ������';
$MESS['MAIN_COUPON_ACTIVATION_TITLE_TEMPLATE'] = '��������� ��������';
$MESS['MAIN_COUPON_ACTIVATION_HEADER_TITLE'] = '��������� ��������';
$MESS['MAIN_COUPON_ACTIVATION_COPYRIGHT'] = '� ��������, #YEAR#. ���������� �������� � �1�-�������24�';